# PHP Captcha

[
	![](https://badgen.net/packagist/v/samy/captcha/latest)
	![](https://badgen.net/packagist/license/samy/captcha)
	![](https://badgen.net/packagist/dt/samy/captcha)
	![](https://badgen.net/packagist/favers/samy/captcha)
](https://packagist.org/packages/samy/captcha)

This is a lean, consistent, and simple way to access Captcha.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/captcha
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-captcha>
* Documentations: <https://monstm.gitlab.io/php-captcha/>
* Annotation: <https://monstm.alwaysdata.net/php-captcha/>
* Issues: <https://gitlab.com/monstm/php-captcha/-/issues>
