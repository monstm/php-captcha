# Captcha Interface

Describes Captcha interface.

---

## verify

Simple captcha verification.

```php
if($captcha->verify()){
	// User verified
}else{
	// Bot Detected
}
```

---

## getReasonPhrase

Gets the response reason phrase associated with the verification status.

```php
$reason_phrase = $captcha->getReasonPhrase();
```
