# Captcha Instance

Simple Captcha Implementation.

---

## hCaptcha

Implementation hCaptcha instance.

```php
$captcha = new HCaptcha($secretkey);
```

### withSiteKey

Return an instance with provided site key.

```php
$captcha = $captcha->withSiteKey($sitekey);
```

### withRemoteIP

Return an instance with provided remote ip.

```php
$captcha = $captcha->withRemoteIP($remote_ip);
```

### withCaptchaResponse

Return an instance with provided captcha response.

In case you do not use the **h-captcha-response** POST parameter to retrieve the token on your server,
then you need to tell the instance classes manually.

```php
$captcha = $captcha->withCaptchaResponse($captcha_response);
```
