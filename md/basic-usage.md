# Basic Usage

You embed the Captcha widget on your site.
For example, on a login form.
The user answers Captcha challenge.
They get a passcode from captcha server that is embedded in your form.
When the user clicks Submit the passcode is sent to your server in the form.
Your server then checks that user passcode sent.
Captcha server says it is valid and credits your account.
Your server now knows the user is not a bot and lets them log in.
Pretty simple!

---

## hCaptcha

The hCaptcha widget can protect your applications from bots, spam, and other forms of automated abuse.
Installing hCaptcha is fast and easy. It requires either adding some simple HTML and server side code,
or using one of the many tools that natively support hCaptcha.

hCaptcha requires two small pieces of client side code to render a captcha widget on an HTML page.

Please see [Composer](https://shortly.win/hcaptcha) for more information.

### JavaScript Library

First, you must include the hCaptcha javascript resource somewhere in your HTML page.
The <script\> must be loaded via HTTPS and can be placed anywhere on the page.
Inside the <head\> tag or immediately after the **.h-captcha** container are both fine.

```html
<script src="https://hcaptcha.com/1/api.js" async defer></script>
```

or you can use link-preload as alternative.

```html
<link rel="preload" as="script" href="https://hcaptcha.com/1/api.js" async defer />
<script src="https://cdn.jsdelivr.net/npm/link-preload@1/link-preload.min.js" defer></script>
```

### DOM Container

Second, you must add an empty DOM container where the hCaptcha widget will be inserted automatically.
The container is typically a <div\> (but can be any element) and must have class **h-captcha** and a **data-sitekey** attribute set to your public site key.


```html
<form method="post" action="[url]">
	<div class="h-captcha" data-sitekey="[hcaptcha-site-key]"></div>
</form>
```

### HTML Skeleton

The complete code can be seen as below:

```html
<html>
	<head>
		...
		<link rel="preload" as="script" href="https://hcaptcha.com/1/api.js" async defer />
		<script src="https://cdn.jsdelivr.net/npm/link-preload@1/link-preload.min.js" defer></script>
		...
	</head>
	<body>
		...
		<form method="post" action="[url]">
			...
			<div class="h-captcha" data-sitekey="[hcaptcha-site-key]"></div>
			...
		</form>
		...
	</body>
</html>
```
