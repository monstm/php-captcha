# Page Interface

Describes Page interface.

---

## withTitle

Return an instance with page title.

```php
$captcha = $captcha->withTitle($title);
```

---

## withFormAction

Return an instance with form action.

```php
$captcha = $captcha->withFormAction($url);
```

---

## withFormData

Return an instance with form data.

In case you need to post custom data.

```php
$captcha = $captcha->withFormData($data);
```

---

## withSponsorship

Return an instance with sponsorship url.

In case you want to promote your awesome sponsor.

```php
$captcha = $captcha->withSponsorship($url);
```

---

## getTemplate

Retrieve html template.

```php
$html = $captcha->getTemplate($theme);

http_response_code(200);
header("Content-Type: text/html");
header("Content-Length: " . strlen($html));
echo($html);
```

| theme      | Description                                |
|:----------:|:-------------------------------------------|
| simple     | Generate Simple captcha page **(default)** |
| cloudflare | Generate Cloudflare captcha page           |
| google     | Generate Google captcha page               |
