<?php

namespace Samy\Captcha;

/**
 * Describes Page interface.
 */
interface PageInterface
{
    /**
     * Return an instance with page title.
     *
     * @param[in] string $Title Page title
     *
     * @return static
     */
    public function withTitle(string $Title): self;


    /**
     * Return an instance with form action.
     *
     * @param[in] string $Url Form Action
     *
     * @return static
     */
    public function withFormAction(string $Url): self;

    /**
     * Return an instance with form data.
     *
     * In case you need to post custom data.
     *
     * @param[in] array $Data Form data
     *
     * @return static
     */
    public function withFormData(array $Data): self;


    /**
     * Return an instance with sponsorship url.
     *
     * In case you want to promote your awesome sponsor.
     *
     * @param[in] string $Url Sponsorship url
     *
     * @return static
     */
    public function withSponsorship(string $Url): self;


    /**
     * Retrieve html template.
     *
     * @param[in] string $Theme Theme name
     *
     * @return string
     */
    public function getTemplate(string $Theme): string;
}
