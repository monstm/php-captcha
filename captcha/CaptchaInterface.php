<?php

namespace Samy\Captcha;

/**
 * Describes Captcha interface.
 */
interface CaptchaInterface
{
    /**
     * Simple site verification.
     *
     * @return bool
     */
    public function verify(): bool;

    /**
     * Gets the response reason phrase associated with the verification status.
     *
     * @return string
     */
    public function getReasonPhrase(): string;
}
