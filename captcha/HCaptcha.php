<?php

namespace Samy\Captcha;

use Exception;
use Samy\Log\Syslog;
use Samy\Psr18\Client;
use Samy\Psr7\Request;
use Samy\Psr7\Stream;
use Samy\Psr7\Uri;

/**
 * Simple hCaptcha implementation.
 */
class HCaptcha extends AbstractCaptcha
{
    private $secret_key = "";
    private $site_key = "";
    private $remote_ip = "";

    private $captcha_response = "";


    /**
     * hCaptcha construction.
     *
     * @param[in] string $SecretKey hCaptcha secret key
     *
     * @return void
     */
    public function __construct(string $SecretKey)
    {
        $this->secret_key = $SecretKey;
    }


    /**
     * Return an instance with provided site key.
     *
     * @param[in] string $SiteKey Site key
     *
     * @return static
     */
    public function withSiteKey(string $SiteKey): self
    {
        $this->site_key = $SiteKey;

        return $this;
    }

    /**
     * Return an instance with provided remote ip.
     *
     * @param[in] string $RemoteIp Remote ip
     *
     * @return static
     */
    public function withRemoteIP(string $RemoteIp): self
    {
        if (($RemoteIp == "") || filter_var($RemoteIp, FILTER_VALIDATE_IP)) {
            $this->remote_ip = $RemoteIp;
        }

        return $this;
    }


    /**
     * Return an instance with provided captcha response.
     *
     * In case you do not use the h-captcha-response POST parameter to retrieve the token on your server,
     * then you need to tell the instance classes manually.
     *
     * @param[in] string $CaptchaResponse hCaptcha response
     *
     * @return static
     */
    public function withCaptchaResponse(string $CaptchaResponse): self
    {
        $this->captcha_response = $CaptchaResponse;

        return $this;
    }


    /**
     * Simple captcha verification.
     *
     * @return bool
     */
    public function verify(): bool
    {
        $ret = false;
        $log = new Syslog();

        try {
            $data = array(
                "secret" => $this->secret_key,
                "response" => ($this->captcha_response != "" ?
                    $this->captcha_response : ($_POST["h-captcha-response"] ?? "")
                )
            );

            $maps = array(
                "sitekey" => $this->site_key,
                "remoteip" => $this->remote_ip
            );

            foreach ($maps as $key => $value) {
                if ($value != "") {
                    $data[$key] = $value;
                }
            }

            $stream = new Stream();
            $stream
                ->withTemp()
                ->write(http_build_query($data));


            $uri = new Uri();
            $uri->parseUrl("https://hcaptcha.com/siteverify");

            $request = new Request();
            $request
                ->withMethod("POST")
                ->withHeader("Content-Type", "application/x-www-form-urlencoded")
                ->withBody($stream)
                ->withUri($uri);

            $client = new Client();
            $response = $client->sendRequest($request);

            if ($response->getStatusCode() == 200) {
                $json = @json_decode($response->getBody()->getContents(), true);

                if ($json) {
                    $ret = ($json["success"] ?? false);
                    $this->reason_phrase = $this->errorDescription(
                        isset($json["error-codes"]) &&
                            is_array($json["error-codes"]) ?
                            $json["error-codes"] :  array()
                    );
                } else {
                    $error = json_last_error_msg();

                    $log->backtrace($error);
                    $this->reason_phrase = $error;
                }
            } else {
                $error = $response->getStatusCode() . " - " . $response->getReasonPhrase();

                $log->backtrace($error);
                $this->reason_phrase = $error;
            }
        } catch (Exception $exception) {
            $log->exception($exception);
            $this->reason_phrase = $exception->getMessage();
        }

        return boolval($ret);
    }

    /**
     * Describe hCaptcha error codes.
     *
     * @param[in] array $ErrorCodes hCaptcha error codes
     *
     * @return string
     */
    private function errorDescription(array $ErrorCodes): string
    {
        $ret = array();

        foreach ($ErrorCodes as $error_code) {
            switch ($error_code) {
                case "missing-input-secret":
                    $description = "Your secret key is missing.";
                    break;
                case "invalid-input-secret":
                    $description = "Your secret key is invalid or malformed.";
                    break;
                case "missing-input-response":
                    $description = "The response parameter (verification token) is missing.";
                    break;
                case "invalid-input-response":
                    $description = "The response parameter (verification token) is invalid or malformed.";
                    break;
                case "bad-request":
                    $description = "The request is invalid or malformed.";
                    break;
                case "invalid-or-already-seen-response":
                    $description = "The response parameter has already been checked, or has another issue.";
                    break;
                case "not-using-dummy-passcode":
                    $description = "You have used a testing sitekey but have not used its matching secret.";
                    break;
                case "sitekey-secret-mismatch":
                    $description = "The sitekey is not registered with the provided secret.";
                    break;
                default:
                    $description = "";
                    break;
            }

            if ($description != "") {
                array_push($ret, $description);
            }
        }

        return implode("; ", $ret);
    }


    /**
     * Retrieve html template.
     *
     * @param[in] string $Theme Theme name
     *
     * @return string
     */
    public function getTemplate(string $Theme): string
    {
        return $this->template("hcaptcha.twig", $Theme);
    }

    /**
     * Retrieve template data.
     *
     * @return array<string, mixed>
     */
    protected function templateData(): array
    {
        return array(
            "sitekey" => $this->site_key,
            "remote_ip" => $this->remote_ip
        );
    }
}
