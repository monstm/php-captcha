<?php

namespace Samy\Captcha;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * This is a simple Captcha implementation that other Captcha can inherit from.
 */
abstract class AbstractCaptcha implements CaptchaInterface, PageInterface
{
    /** describe reason phrase */
    protected $reason_phrase = "";


    /** describe page title */
    protected $page_title = "";

    /** describe form action */
    protected $form_action = "";

    /** describe form data */
    protected $form_data = array();

    /** describe sponsorship url */
    protected $sponsorship_url = "";


    /**
     * Retrieve template data.
     *
     * @return array<string, mixed>
     */
    abstract protected function templateData(): array;


    /**
     * Gets the response reason phrase associated with the verification status.
     *
     * @return string
     */
    public function getReasonPhrase(): string
    {
        return $this->reason_phrase;
    }


    /**
     * Return an instance with page title.
     *
     * @param[in] string $Title Page title
     *
     * @return static
     */
    public function withTitle(string $Title): self
    {
        $this->page_title = $Title;

        return $this;
    }


    /**
     * Return an instance with form action.
     *
     * @param[in] string $Url Form Action
     *
     * @return static
     */
    public function withFormAction(string $Url): self
    {
        if (($Url == "") || filter_var($Url, FILTER_VALIDATE_URL)) {
            $this->form_action = $Url;
        }

        return $this;
    }

    /**
     * Return an instance with form data.
     *
     * In case you need to post custom data.
     *
     * @param[in] array $Data Form data
     *
     * @return static
     */
    public function withFormData(array $Data): self
    {
        $this->form_data = array();

        foreach ($Data as $name => $value) {
            if (is_string($name)) {
                switch (strtolower(gettype($value))) {
                    case "boolean":
                        $this->form_data[$name] = ($value ? "true" : "false");
                        break;
                    case "integer":
                    case "double":
                        $this->form_data[$name] = strval($value);
                        break;
                    case "string":
                        $this->form_data[$name] = htmlentities($value);
                        break;
                }
            }
        }


        return $this;
    }


    /**
     * Return an instance with sponsorship url.
     *
     * In case you want to promote your awesome sponsor.
     *
     * @param[in] string $Url Sponsorship url
     *
     * @return static
     */
    public function withSponsorship(string $Url): self
    {
        if (($Url == "") || filter_var($Url, FILTER_VALIDATE_URL)) {
            $this->sponsorship_url = $Url;
        }

        return $this;
    }


    /**
     * Generate html template.
     *
     * @param[in] string $Template Template name
     * @param[in] string $Theme Theme name
     *
     * @return string
     */
    protected function template(string $Template, string $Theme): string
    {
        $loader = new FilesystemLoader(dirname(__DIR__) . DIRECTORY_SEPARATOR . "twig");
        $environment = new Environment($loader, array(
            "debug" => false,
            "defaults" => "utf-8",
            "cache" => false,
            "auto_reload" => false,
            "strict_variables" => false,
            //"autoescape" => false,
            "optimizations" => -1
        ));

        return $environment->render("template/" . $Template, array_merge(
            $this->templateTimes(),
            $this->templateUri(),
            array(
                "theme" => $this->templateTheme($Theme),

                "title" => $this->page_title,
                "form_action" => $this->form_action,
                "form_data" => $this->form_data,
                "sponsorship_url" => $this->sponsorship_url,

                "client_ip" => ($_SERVER["REMOTE_ADDR"] ?? "0.0.0.0")
            ),
            $this->templateData()
        ));
    }

    /**
     * Retrieve template theme.
     *
     * @param[in] string $Theme Theme name
     *
     * @return string
     */
    private function templateTheme(string $Theme): string
    {
        switch (strtolower(trim($Theme))) {
            case "cloudflare":
                $ret = "cloudflare.twig";
                break;
            case "google":
                $ret = "google.twig";
                break;
            default:
                $ret = "simple.twig";
                break;
        }

        return $ret;
    }


    /**
     * Retrieve template times.
     *
     * @return array<string, string>
     */
    private function templateTimes(): array
    {
        $time = time();
        $ret = array("time_server" => date("Y-m-d H:i:s", $time));
        $times = array(
            "time_atom" => "DATE_ATOM",
            "time_cookie" => "DATE_COOKIE",
            "time_iso8601" => "DATE_ISO8601",
            "time_rfc822" => "DATE_RFC822",
            "time_rfc8509" => "DATE_RFC8509",
            "time_rfc1036" => "DATE_RFC1036",
            "time_rfc1123" => "DATE_RFC1123",
            "time_rfc7231" => "DATE_RFC7231",
            "time_rfc2822" => "DATE_RFC2822",
            "time_rfc3339" => "RFC3339",
            "time_rss" => "DATE_RSS",
            "time_w3c" => "DATE_W3C"
        );

        foreach ($times as $key => $name) {
            if (defined($name)) {
                $ret[$key] = date(strval(constant($name)), $time);
            }
        }


        return $ret;
    }

    /**
     * Retrieve template uri.
     *
     * @return array<string, string>
     */
    private function templateUri(): array
    {
        $request_scheme = ($_SERVER["REQUEST_SCHEME"] ?? "http");
        $http_host = ($_SERVER["HTTP_HOST"] ?? "localhost");
        $request_uri = ($_SERVER["REQUEST_URI"] ?? "/");

        return array(
            "request_scheme" => $request_scheme,
            "http_host" => $http_host,
            "request_uri" => $request_uri,

            "current_url" => $request_scheme . "://" . $http_host . $request_uri
        );
    }
}
