<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Samy\Captcha\AbstractCaptcha;

abstract class AssertCaptcha extends TestCase
{
    protected function assertCaptcha(AbstractCaptcha &$Captcha, bool $IsVerified, string $ReasonPhrase): void
    {
        $this->assertSame($IsVerified, $Captcha->verify());
        $this->assertSame($ReasonPhrase, $Captcha->getReasonPhrase());
    }
}
