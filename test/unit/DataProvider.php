<?php

namespace Test\Unit;

use Samy\Dummy\Random;

class DataProvider
{
    // $SecretKey, $SiteKey, $CaptchaResponse, $IsVerified, $ReasonPhrase
    public function dataHCaptcha(): array
    {
        $random = new Random();

        return array(
            array(
                "",
                "",
                "",
                false,
                "The response parameter (verification token) is missing.; Your secret key is missing."
            ),
            array(
                $random->string(),
                "",
                "",
                false,
                "The response parameter (verification token) is missing."
            ),
            array(
                "",
                $random->string(),
                "",
                false,
                "The response parameter (verification token) is missing.; Your secret key is missing."
            ),
            array(
                "",
                "",
                $random->string(),
                false,
                "Your secret key is missing.; The response parameter (verification token) is invalid or malformed."
            ),
            array(
                $random->string(),
                $random->string(),
                "",
                false,
                "The response parameter (verification token) is missing."
            ),
            array(
                "",
                $random->string(),
                $random->string(),
                false,
                "Your secret key is missing.; The response parameter (verification token) is invalid or malformed."
            ),
            array(
                $random->string(),
                "",
                $random->string(),
                false,
                "The response parameter (verification token) is invalid or malformed."
            ),
            array(
                $random->string(),
                $random->string(),
                $random->string(),
                false,
                "The response parameter (verification token) is invalid or malformed."
            ),
            array(
                "0x0000000000000000000000000000000000000000",
                "10000000-ffff-ffff-ffff-000000000001",
                "10000000-aaaa-bbbb-cccc-000000000001",
                true,
                ""
            ),
            array(
                "0x0000000000000000000000000000000000000000",
                "20000000-ffff-ffff-ffff-000000000002",
                "20000000-aaaa-bbbb-cccc-000000000002",
                true,
                ""
            ),
            array(
                "0x0000000000000000000000000000000000000000",
                "30000000-ffff-ffff-ffff-000000000003",
                "30000000-aaaa-bbbb-cccc-000000000003",
                true,
                ""
            )
        );
    }
}
