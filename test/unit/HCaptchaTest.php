<?php

namespace Test\Unit;

use Samy\Captcha\HCaptcha;

class HCaptchaTest extends AssertCaptcha
{
    /**
     * @dataProvider \Test\Unit\DataProvider::dataHCaptcha
     */
    public function testCaptcha($SecretKey, $SiteKey, $CaptchaResponse, $IsVerified, $ReasonPhrase): void
    {
        $captcha = new HCaptcha($SecretKey);

        $this->assertInstanceOf(
            HCaptcha::class,
            $captcha
                ->withSiteKey($SiteKey)
                ->withCaptchaResponse($CaptchaResponse)
        );

        $this->assertCaptcha($captcha, $IsVerified, $ReasonPhrase);
    }
}
